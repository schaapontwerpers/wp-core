<?php
/*
Plugin Name:  WP Core
Plugin URI:   https://packagist.org/packages/schaapdigitalcreatives/wp-core
Description:  The core WordPress plugin for Schaap Digital Creatives
Version:      1.3.0
Author:       Schaap Digital Creatives
Author URI:   https://schaapontwerpers.nl/
License:      GNU General Public License
*/

new WordPressCore\Init();
