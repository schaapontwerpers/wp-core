<?php

/**
 * Changes to User Capabilities
 */

namespace WordPressCore\Users;

use WordPressPluginAPI\ActionHook;
use WordPressPluginAPI\FilterHook;

class Capabilities implements ActionHook, FilterHook
{
    /**
     * Subscribe functions to corresponding actions
     */
    public static function getActions(): array
    {
        return [
            'editable_roles' => 'userRoleDropdown',
            'init' => 'editorCapabilities',
        ];
    }

    /**
     * Subscribe functions to corresponding filters
     */
    public static function getFilters(): array
    {
        return [
            'map_meta_cap' => array('mapMetaCap', 10, 4),
        ];
    }

    /**
     * Set editor capabilities so that they can add and delete users
     */
    public function editorCapabilities()
    {
        $editor = get_role('editor');
        $editor->add_cap('manage_network_users');
        $editor->add_cap('edit_theme_options');
        $editor->add_cap('create_users');
        $editor->add_cap('edit_users');
        $editor->add_cap('delete_users');
        $editor->add_cap('promote_users');
        $editor->add_cap('list_users');

        $administrator = get_role('administrator');
        $administrator->add_cap('manage_network_users');
    }

    /**
     * Remove capability for anyone else than an admin to remove administrators
     */
    public function mapMetaCap(
        array $caps,
        string $cap,
        int $ID,
        array $args
    ): array {
        switch ($cap) {
            case 'edit_user':
            case 'remove_user':
            case 'promote_user':
                if (isset($args[0]) && $args[0] == $ID) {
                    break;
                } elseif (!isset($args[0])) {
                    $caps[] = 'do_not_allow';
                }

                $other = new \WP_User(absint($args[0]));
                if ($other->has_cap('administrator')) {
                    if (!current_user_can('administrator')) {
                        $caps[] = 'do_not_allow';
                    }
                }

                break;

            case 'delete_user':
            case 'delete_users':
                if (!isset($args[0])) {
                    break;
                }

                $other = new \WP_User(absint($args[0]));
                if ($other->has_cap('administrator')) {
                    if (!current_user_can('administrator')) {
                        $caps[] = 'do_not_allow';
                    }
                }

                break;

            default:
                break;
        }

        return $caps;
    }

    /**
     * Don't allow editors to make users admin
     */
    public function userRoleDropdown($allRoles)
    {
        global $pagenow;

        // if current user is editor AND current page is edit/new user page
        if (
            !current_user_can('administrator')
            && ($pagenow == 'user-edit.php' || $pagenow == 'user-new.php')
        ) {
            unset($allRoles['administrator']);
        }

        return $allRoles;
    }
}
