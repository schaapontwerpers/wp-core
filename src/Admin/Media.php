<?php

/**
 * Change media settings
 */

namespace WordPressCore\Admin;

use WordPressPluginAPI\ActionHook;
use WordPressPluginAPI\FilterHook;

class Media implements ActionHook, FilterHook
{
    /**
     * Subscribe functions to corresponding actions
     *
     * @return array
     */
    public static function getActions(): array
    {
        return [
            'init' => 'updateImageSizes',
        ];
    }

    /**
     * Subscribe functions to corresponding actions
     *
     * @return array
     */
    public static function getFilters(): array
    {
        return [
            'acf/format_value/type=image' => ['changeImageOutput', 20, 3],
            'acf/settings/rest_api_format' => 'setOutputFormat',
            'big_image_size_threshold' => 'disableThreshold',
            'upload_mimes' => 'addSvgMime',
            'wp_check_filetype_and_ext' => [
                'checkFiletype',
                100,
                4,
            ],
        ];
    }

    /**
     * SVG for Media library
     *
     * @param array $mimes List of mime types
     *
     * @return array
     */
    public function addSvgMime(array $mimes): array
    {
        $mimes['svg'] = 'application/svg+xml';

        return $mimes;
    }

    /**
     * Set SVG to image/svg+xml after first check
     *
     * @param array  $data     Data belonging to the file
     * @param string $fileTmp  Temp file name
     * @param string $fileName Name of the file
     * @param mixed  $mimes    List of mime types
     *
     * @return array
     */
    public function checkFiletype(
        array $data,
        string $fileTmp,
        string $fileName,
        $mimes
    ): array {
        if (substr($fileName, -4) === '.svg') {
            $data['ext'] = 'svg';
            $data['type'] = 'image/svg+xml';
        }

        return $data;
    }

    /**
     * Change the output of all image fields
     *
     * @param mixed $value  The original value
     * @param int   $postId The current post ID
     * @param array $field  The original field
     *
     * @return mixed
     */
    public function changeImageOutput($value, $postId, $field)
    {
        $attr = $value;

        if ($field['return_format'] === 'array' && is_array($value)) {
            $imageId = $value['ID'];
            $image = wp_get_attachment_image_src($imageId, 'full');

            if ($image) {
                list($src, $width, $height) = $image;

                $attr = array(
                    'id' => (int)$imageId,
                    'url' => $src,
                    'width' => $width,
                    'height' => $height,
                );

                $imageMeta = wp_get_attachment_metadata($imageId);

                if (is_array($imageMeta) && array_key_exists('sizes', $imageMeta)) {
                    $attr['sizes'] = [
                        'full' => [
                            'url' => $src,
                            'width' => $width,
                            'height' => $height,
                        ],
                    ];

                    foreach ($imageMeta['sizes'] as $size => $sizeData) {
                        if ($size !== 'thumbnail') {
                            $attr['sizes'][$size] = [
                                'height' => $sizeData['height'],
                                'width' => $sizeData['width'],
                                'url' => wp_get_attachment_image_url(
                                    $imageId,
                                    $size,
                                ),
                            ];
                        }
                    }
                }

                $value = $attr;
            }
        }

        return $attr;
    }

    /**
     * Disable the big treshold, this is already handled by optimizing the image
     *
     * @return bool
     */
    public function disableThreshold(): bool
    {
        return false;
    }

    /**
     * SVG for Media library
     *
     * @return void
     */
    public function updateImageSizes()
    {
        // Set new values to default image sizes
        update_option('medium_size_w', 640);
        update_option('medium_size_h', 0);
        update_option('medium_large_size_w', 1280);
        update_option('medium_large_size_h', 0);
        update_option('large_size_w', 1920);
        update_option('large_size_h', 0);

        // Remove some image sizes
        remove_image_size('2048x2048');
        remove_image_size('1536x1536');
    }

    /**
     * Set the correct default output format
     *
     * @return string
     */
    public function setOutputFormat(): string
    {
        return 'standard';
    }
}
