<?php

namespace WordPressCore\Admin;

use WordPressPluginAPI\ActionHook;

class Setup implements ActionHook
{
    /**
     * Subscribe functions to corresponding actions
     */
    public static function getActions(): array
    {
        return [
            'after_setup_theme' => 'updateFeatures',
        ];
    }

    /**
     * Clean the WP Head of unecessary tags and add title support
     */
    public function updateFeatures()
    {
        add_theme_support('post-thumbnails');

        // Remove unecessary links in Head
        remove_filter('the_excerpt', 'wpautop');
        remove_theme_support('core-block-patterns');
    }
}
