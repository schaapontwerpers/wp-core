<?php

namespace WordPressCore\Admin;

use WordPressPluginAPI\ActionHook;
use WordPressPluginAPI\FilterHook;

class Menu implements ActionHook, FilterHook
{
    /**
     * Subscribe functions to corresponding actions
     */
    public static function getActions(): array
    {
        return [
            'admin_menu' => 'cleanMenu',
        ];
    }

    /**
     * Subscribe functions to corresponding filters
     */
    public static function getFilters(): array
    {
        return [
            'custom_menu_order' => 'customMenuOrder',
            'menu_order' => 'menuOrder',
        ];
    }

    /**
     * Remove unused menu pages because it is never used
     */
    public function cleanMenu(): void
    {
        // Remove submenu pages
        global $submenu;
        unset($submenu['themes.php'][5]); // Themes
        unset($submenu['themes.php'][7]); // Customizer
        unset($submenu['options-general.php'][30]); // Media

        remove_menu_page('edit-comments.php');
    }

    /**
     * Register custom menu order function
     */
    public function customMenuOrder(): bool
    {
        return true;
    }

    /**
     * Reorder the menu so that it's Dashboard, Media, Pages, Posts
     */
    public function menuOrder(): array
    {
        return [
            'index.php',
            'kinsta-tools',
            'upload.php',
            'separator1',
            'edit.php?post_type=structure',
            'edit.php?post_type=page',
            'edit.php',
        ];
    }
}
