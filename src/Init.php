<?php

namespace WordPressCore;

use WordPressPluginAPI\Manager;

class Init
{
    private $hooks;

    public function __construct()
    {
        $this->hooks = [
            new Admin\Media(),
            new Admin\Menu(),
            new Admin\Setup(),
            new Mail\Settings(),
            new RestAPI\ACF(),
            new RestAPI\Cache(),
            new Users\Capabilities(),
        ];

        // Run PluginAPI registration
        $manager = new Manager();
        foreach ($this->hooks as $class) {
            $manager->register($class);
        }
    }
}
