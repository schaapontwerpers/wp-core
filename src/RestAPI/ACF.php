<?php

namespace WordPressCore\RestAPI;

use WordPressPluginAPI\FilterHook;

class ACF implements FilterHook
{
    /**
     * Subscribe functions to corresponding filters
     */
    public static function getFilters(): array
    {
        return [
            'acf/settings/rest_api_format' => 'setOutputFormat',
        ];
    }

    /**
     * Set the correct default output format
     */
    public function setOutputFormat(): string
    {
        return 'standard';
    }
}
