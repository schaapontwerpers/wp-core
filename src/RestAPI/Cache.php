<?php

/**
 * Abstract class used for all routes
 */

namespace WordPressCore\RestAPI;

use WordPressPluginAPI\ActionHook;
use WordPressPluginAPI\FilterHook;

class Cache implements ActionHook, FilterHook
{
    /**
     * The namespace used in all wp-json routes
     */
    private $namespace = 'sdc/v2';

    /**
     * Add filters
     */
    public static function getActions(): array
    {
        return array(
            'mu_plugins_loaded' => 'filterRequest',
        );
    }

    /**
     * Add filters
     */
    public static function getFilters(): array
    {
        return array(
            'wp_rest_cache/allowed_endpoints' => [
                'addEndpoints',
                10,
                1,
            ],
            'wp_rest_cache/settings_capability' => [
                'changeCapabilities',
                10,
                1,
            ],
            'wp_rest_cache/determine_object_type' => [
                'determineObjectType',
                10,
                4,
            ],
        );
    }

    /**
     * Add custom endpoints
     */
    public function addEndpoints(array $allowedEndpoints): array
    {
        $returnEndpoints = [];

        if (defined('WP_ENV') && WP_ENV !== 'development') {
            $returnEndpoints = $allowedEndpoints;

            if (
                !isset($returnEndpoints[$this->namespace])
                || !in_array('app', $returnEndpoints[$this->namespace])
            ) {
                $returnEndpoints[$this->namespace][] = 'app';
            }
        }

        return $returnEndpoints;
    }

    /**
     * Change capabilities
     */
    public function changeCapabilities($capability): string
    {
        return 'edit_posts';
    }

    /**
     * Determine the correct object type
     */
    public function determineObjectType(
        string $objectType,
        string $cacheKey,
        array $data,
        string $uri
    ): string {
        if ($objectType !== 'unknown') {
            return $objectType;
        }

        // Do your magic here
        $split = explode('?', $uri);

        if ($split[0]) {
            $withoutBase = str_replace('/wp-json/wp/v2/', '', $split[0]);

            $postTypes = get_post_types_by_support(['editor']);
            $restBases = array_map(function ($postType) {
                $postTypeObject = get_post_type_object($postType);

                return $postTypeObject->rest_base;
            }, $postTypes);

            $postTypesByRestBase = array_combine($restBases, $postTypes);

            if (array_key_exists($withoutBase, $postTypesByRestBase)) {
                $objectType = $postTypesByRestBase[$withoutBase];
            }
        }

        return $objectType;
    }
}
