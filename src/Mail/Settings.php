<?php

/**
 * Set mail to SMTP method
 *
 * PHP version 8
 *
 * @package WordPressCore\Mail\Settings
 * @author  SchaapOntwerpers <info@schaapontwerpers.nl>
 * @license https://opensource.org/licenses/GPL-3.0 GNU General Public License
 */

namespace WordPressCore\Mail;

use WordPressPluginAPI\FilterHook;

class Settings implements FilterHook
{
    /**
     * Subscribe functions to corresponding actions
     */
    public static function getFilters(): array
    {
        return [
            'wp_mail_charset' => 'setCharset',
            'wp_mail_content_type' => 'setContentType',
        ];
    }

    /**
     * Set charset to utf-8
     */
    public function setCharset(): string
    {
        return 'UTF-8';
    }

    /**
     * Set content type to html
     */
    public function setContentType(): string
    {
        return 'text/html';
    }
}
